package main

import (
    "encoding/json"
    "log"
    "net/http"
    "github.com/gorilla/mux"
    "fmt"
)

type RespFunc func(http.ResponseWriter, *http.Request)

type Router struct {
  m map[string] RespFunc
}

func (router *Router) AddRoute(s string, f RespFunc) {
  router.m[s] = f
}

func (router Router) ServeHTTP(w http.ResponseWriter, r *http.Request) {
  fmt.Print(r.URL)
  //router.f("hello2")
  if val, ok := router.m[fmt.Sprintf("%v", r.URL)]; ok {
    val(w, r)
  } else {
    do404(w, r)
  }

  //GetPeople(w, r)
}

type Person struct {
    ID        string   `json:"id,omitempty"`
    Firstname string   `json:"firstname,omitempty"`
    Lastname  string   `json:"lastname,omitempty"`
    Address   *Address `json:"address,omitempty"`
}
type Address struct {
    City  string `json:"city,omitempty"`
    State string `json:"state,omitempty"`
}

var people []Person




func GetPeople(w http.ResponseWriter, r *http.Request) {
    json.NewEncoder(w).Encode(people)
}

func GetPeople2(w http.ResponseWriter, r *http.Request) {
    fmt.Print("people2 function\n")
    json.NewEncoder(w).Encode(people)
}

func do404(w http.ResponseWriter, r *http.Request) {
    fmt.Print("404funn\n")
    json.NewEncoder(w).Encode("4 oh 4, bruh")
}

func add_routes(r *mux.Router) {
  r.HandleFunc("/people", GetPeople).Methods("GET")
}

func add_peeps() {
  people = append(people, Person{ID: "1", Firstname: "John", Lastname: "Doe", Address: &Address{City: "City X", State: "State X"}})
  people = append(people, Person{ID: "2", Firstname: "Koko", Lastname: "Doe", Address: &Address{City: "City Z", State: "State Y"}})
  people = append(people, Person{ID: "3", Firstname: "Francis", Lastname: "Sunday"})
}
// our main function
func main() {
    add_peeps()
    router := mux.NewRouter()
    add_routes(router)
    rx := Router{make(map[string] RespFunc)}
    rx.AddRoute("/peoplex", GetPeople2)
    log.Fatal(http.ListenAndServe(":8000", rx))
}
